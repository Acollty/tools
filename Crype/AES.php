<?php

/**
 * Class AES
 * 动态加密类，用于防止盗链
 */
class AES
{
    private $method;
    private $key;
    public function __construct()
    {
        $this->method = "AES-128-CBC";
        $this->key = "__MEC__";
    }

    public function Encrypt($strData)
    {
        $strDesKey = $this->key;
        $strMethod = $this->method;
        $intIvLength = openssl_cipher_iv_length($strMethod);
        $strIv = openssl_random_pseudo_bytes($intIvLength);
        $strEncryptData = openssl_encrypt($strData,$strMethod,$strDesKey,OPENSSL_RAW_DATA,$strIv);
        return base64_encode($strIv.$strEncryptData);
    }

    public function Decrypt($strEncryptData)
    {
        $strDesKey = $this->key;
        $strMethod = $this->method;
        $strEncryptData = base64_decode($strEncryptData);
        $intIvLength = openssl_cipher_iv_length($strMethod);
        $strIv = substr($strEncryptData, 0, $intIvLength);
        return openssl_decrypt(substr($strEncryptData, $intIvLength), $strMethod, $strDesKey, OPENSSL_RAW_DATA, $strIv);
    }
}

//测试
$aes = new AES();
$str = '20190521/LDaUXFWC/mp4/LDaUXFWC.mp4';
echo '原始数据：'.$str;
echo PHP_EOL;

$strCrypt = $aes->Encrypt($str);
echo '加密后的数据：'.$strCrypt;
echo PHP_EOL;

$strDecrypt = $aes->Decrypt($strCrypt);
echo '解密后的数据：'.$strDecrypt;
echo PHP_EOL;


